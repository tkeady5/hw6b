#include "CTree.h"
#include <string>
#include <sstream>

#include <iostream>

using::std::string;
using::std::cout;
using::std::endl;

/*
 * Basic constructor puts the character as the data of this node
 */
//CTree::CTree(char ch) : data(ch) {}

/*
 * Real constructor has ability to add additional information about this node
 */
CTree::CTree(char ch, CTree* k, CTree* s, CTree* p) : data(ch), kids(k), sibs(s), prev(p) {}


/*
 * Desctructor deletes the last child of the last sibling, the 
 * second to last child and so on until it deletes the sibling 
 * itself, then moves on to the second to last sibling and repeats
 * until it reaches the root node
 */
CTree::~CTree() {
	//printf("Removing\n");
	delete kids;
	delete sibs;
	
	/*
	if (kids) {
		//printf("Removing %c\n", this->data);
		printf("\tinside if\n");
		delete kids;
	}*/
}


bool CTree::addSibling(char ch) {
	CTree newOne = CTree(ch);
	return this->addSibling(&newOne);
}

bool CTree::addSibling(CTree* root) {
	if (/*(root->sibs) ||*/ (root->prev) || !(this->prev)) {
		return false;				// Check for invalidity
	} else if (this->data == root->data) {
		return false;
	} else if (this->data > root->data) {		// If its next alphabetically
		if (this->prev->sibs == this) {		// and it is under a sibling
			//REORDER
			root->prev = this->prev;	// 
			root->sibs = this->prev->sibs;
			this->prev->sibs = root;
			this->prev = root;
			return true;
		} else {
			root->prev = prev;
			prev->kids = root;
			root->sibs = this;
			return true;
		}
	} else if (sibs == NULL) {
		sibs = root;
		root->prev = this;
		return true;
		
	} else {		// this->data < root->data
		return (this->sibs)->addSibling(root);
	}
			
}





/*{
		if (root->data > this->data) {	// Like root->data = 'a' and this->data = 'b'
			//if (this->sibs->data 
			return (this->sibs)->addSibling(root);
		} else if (root->data < this->data) {	// Once it passes it in the alphabet
			//root->sibs = this->sibs;	// Point it to the rest of the list
			this->sibs = root->sibs;		// Add it as a sib
			root->sibs = this;		// Point it to the rest of the list
			//this->sibs = root;		// Add it as a sib
			//if (this->data != this->sibs->data) {
				if (this->data == root->data) {
					return false;
				}
				root->sibs = this->sibs;
				if (this->sibs->data) {
					if (this->sibs->data == root->data) {
						return false;
					}
				}
				this->sibs = root;
				//root->prev = this->prev;		// Point to its prev
				root->prev = this;		// Point to its prev
				
				return true;
			//}
		} else {	// (root->data == this->data)
			return false;
		}
	}
	
}
*/		

/*
 * 
 */
bool CTree::addChild(char ch) {
	CTree* newOne = new CTree(ch);	// Want a new one
	/*bool good = newOne->addChild();
	if (!good) { delete newOne; }; */
	return this->addChild(newOne);	// call the pointer function
}

bool CTree::addChild(CTree* root) {
	if ((root->sibs) || (root->prev)) {
		cout << "didnt add" << endl;
		return false;	// Shouldnt have any 
	} else {
		if (this->kids) {				// If it has kids,
			return (this->kids)->addSibling(root);	// start looking through them
		} else {					// Otherwise,
			/*this->kids = root;*/			// Make this its kid
			root->prev = this;
			//root->kids = this->kids;
			this->kids = root;
			//cout << "added child " << this->data << endl;
			return true;
		}
	}
	
	
}
	
	
/*		if (root->data < this->data) {	// Like root->data = 'a' and this->data = 'b'
			return (this->sibs).addChild(root);
		} else if (root->data > this->data) {	// Once it passes it in the alphabet
			this->kids = root;		// Add it as a kid
			root->prev = &this;		// Point to its prev
			root->sibs = this->kids;	// point it to the rest of the list
		} else {	// (root->data == this->data)
			return false;
		}
	}
*/	
	

string CTree::toString() {
	std::ostringstream out;
	this->toString(out);
	return out.str();
}

void CTree::toString(std::ostringstream& out) {
	//std::ostringstream out;
	//if (this->data) {
		out << this->data << std::endl;
	//} else {
	//	return out.str();
	//} 			// this is how they have it in pseudocode
	if (this->kids) {
		(this->kids)->toString(out);
	}
	//} else if (this->sibs) {
	if (this->sibs) {
		(this->sibs)->toString(out);
	}	

}	






